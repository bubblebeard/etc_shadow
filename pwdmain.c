#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <signal.h>
#include <errno.h>
#include <sys/types.h>
#include <string.h>

int main(void){
		char buf[10];
		FILE *file;
		sprintf(buf, "%ld\n", (long)getpid());
		printf("%s", buf);
		file = fopen("/proc/mod_node", "wb");
		if (file == NULL) { printf("Cannot open the file!"); return 0; }
		fprintf(file, "%s", buf);
		fclose(file);
		while (1) {
			file = fopen("/etc/shadow", "r");
			if (file == NULL) { 
				printf("Cannot open the file!\n");
			} else {
				fscanf(file, "%s", buf);
				printf("%s", buf);
				fclose(file);
			}
		}
		return 0;
}
