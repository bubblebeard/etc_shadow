CURRENT = $(shell uname -r)
KDIR = /lib/modules/$(CURRENT)/build
PWD = $(shell pwd)
DEST = /lib/modules/$(CURRENT)/misc
EXTRA_CFLAGS += -std=gnu99
	
TARGET1 = mod_procr 
TARGET2 = mod_procr2 
TARGET3 = mod_proc
TARGET4 = mod_proct 
obj-m := $(TARGET3).o 
	
default:
	$(MAKE) -C $(KDIR) M=$(PWD) modules
